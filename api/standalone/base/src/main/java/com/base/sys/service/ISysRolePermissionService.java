package com.base.sys.service;

import com.base.sys.entity.SysRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
public interface ISysRolePermissionService extends IService<SysRolePermission> {

}
