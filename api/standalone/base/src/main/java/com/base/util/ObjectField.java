package com.base.util;


import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ObjectField {

    String value() default "";

    String dicType() default "";
}
