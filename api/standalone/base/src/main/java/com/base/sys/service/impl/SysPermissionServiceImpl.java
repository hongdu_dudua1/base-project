package com.base.sys.service.impl;

import com.base.sys.dto.PermissionDTO;
import com.base.sys.entity.SysPermission;
import com.base.sys.mapper.SysPermissionMapper;
import com.base.sys.service.ISysPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.base.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements ISysPermissionService {

    @Autowired
    private SysPermissionMapper sysPermissionMapper;

    @Override
    public Result permissionList(){
        List<PermissionDTO> permissionDTOS = sysPermissionMapper.permissionList();
        return Result.successResult(permissionDTOS);
    }

}
