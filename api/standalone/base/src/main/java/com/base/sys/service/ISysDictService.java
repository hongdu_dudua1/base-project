package com.base.sys.service;

import com.base.sys.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.util.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-11-30
 */
public interface ISysDictService extends IService<SysDict> {

    Result getTables( String tableName);
}
