<template>
    <div class="app-container">
        <div class="filter-container">
            <el-input   style="width: 200px;" class="filter-item"/>
            <el-button class="filter-item" @click="getListData({page:1,limit:10})" type="primary" icon="el-icon-search">查询
            </el-button>
            <el-button class="filter-item"  type="primary" icon="el-icon-edit" @click="dialogFormVisible=true">新增</el-button>
        </div>
        <br>
        <br>
        <el-table
                v-loading="listLoading"
                :data="list"
                border
                fit
                highlight-current-row
                style="width: 100%;">

            <#list table.fields as field>
               <el-table-column label="${field.comment}" align="center">
                   <template slot-scope="scope">
                       <span>{{ scope.row.${field.propertyName} }}</span>
                   </template>
               </el-table-column>
            </#list>


            <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
                <template slot-scope="scope">
                    <el-button type="primary" size="mini"   @click="toEdit(scope.row)">
                        编辑
                    </el-button>
                    <el-button size="mini" type="danger"   :loading="scope.row.loading" @click="deleteObj(scope.row)">
                        删除
                    </el-button>
                </template>
            </el-table-column>
        </el-table>

        <pagination v-show="pageQuery.total>0" :total="pageQuery.total" :page.sync="pageQuery.page"
                    :limit.sync="pageQuery.limit" @pagination="getListData"/>


        <el-dialog title="编辑" :visible.sync="dialogFormVisible">
            <el-form ref="dataForm" :rules="rules" :model="obj" label-position="left">
                <#list table.fields as field>
                    <el-form-item label="${field.comment}" prop="${field.propertyName}">
                        <el-input v-model="obj.${field.propertyName}" placeholder="请输入${field.comment}"/>
                    </el-form-item>
                </#list>
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="dialogFormVisible = false">取消</el-button>
                <el-button type="primary" :loading="btnLoading" @click="editData">确定</el-button>
            </div>
        </el-dialog>
    </div>
</template>

<script>
    import {getList, saveOrUpdate, deleteObjs} from '@/api/${package.ModuleName}/${entity}';
    export default{
        data(){
            return {
                list: [],
                listLoading: false,
                dialogFormVisible: false,
                btnLoading: false,
                obj: {
                      <#list table.fields as field>
                          ${field.propertyName} : "",
                      </#list>
                },
                rules: {
                     <#list table.fields as field>
                         ${field.propertyName} : [{required: true, message: '请填写${field.comment}', trigger: 'change'}],
                     </#list>
                },
                pageQuery: {
                    total: 0,
                    page: 1,
                    limit: 10
                }
            }
        },
        watch: {
            dialogFormVisible(val){
                if (!val) {
                    this.obj = {
                           <#list table.fields as field>
                               ${field.propertyName} : "",
                           </#list>
                    }
                    this.$refs["dataForm"].resetFields();
                }
            },
        },
        mounted(){
            this.getListData(this.pageQuery);
        },
        methods: {
            deleteObj(row){
                var array = [];
                 <#list table.fields as field>
                     <#if field.keyFlag>
                        array.push(row.${field.propertyName});
                     </#if>
                 </#list>
                row.loading = true;
                deleteObjs(array).then(res => {
                    row.loading = false;
                if (res.code == 0) {
                    this.getListData({page: 1, limit: 10});
                }
            })
            },
            getListData(pageParam){
                this.listLoading = true;
                getList(pageParam).then(res => {
                    this.listLoading = false;
                if (res.code == 0) {
                    this.list = res.value.records;
                    this.pageQuery.total = res.value.total;
                    this.pageQuery.page = res.value.current;
                    this.pageQuery.limit = res.value.size;
                }
            })
            },
            toEdit(row){
                this.dialogFormVisible = true;
                this.obj = JSON.parse(JSON.stringify(row));
            },
            editData(){
                this.$refs['dataForm'].validate((valid) => {
                    if (valid) {
                        this.btnLoading = true;
                        saveOrUpdate(this.obj).then((res) => {
                            this.btnLoading = false;
                        if (res.code == 0) {
                            this.dialogFormVisible = false
                            this.$message({
                                message: '编辑成功',
                                type: 'success'
                            })
                            this.getListData({page: 1, limit: 10});
                        }
                    })
                    }
                })
            }
        }
    }
</script>

<style>
</style>
