import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

export const constantRouterMap = [
  {path: '/login', component: () => import('@/views/login/index'), hidden: true},
  {path: '/404', component: () => import('@/views/404'), hidden: true},
  {
    path:"/user",
    component: Layout,
    redirect:"/user/info",
    name: '个人信息',
    children: [{
      path: 'info',
      name: '个人信息',
      meta: {
        title: "个人信息",
      },
      component: () => import('@/views/sys/userInfo')
    }],
    hidden:true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: '首页',
    meta:{
      icon: "dashboard"
    },

    children: [{
      path: 'dashboard',
      name:"首页",
      meta: {
        title: "首页",
      },
      component: () => import('@/views/dashboard/index')
    }]
  },
  {path: '*', component: () => import('@/views/404'), hidden: true}
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap
})

//需要鉴权、动态匹配的路由
export const asyncRouterMap = [
  {
    path: '/system',
    name: '系统管理',
    component: Layout,
    redirect: "/system/employee",
    meta: {
      title: "系统管理",
      icon: "nested"
    },
    children: [
      {
        path: 'employee',
        name: '系统用户管理',
        component: ()=> import("@/views/sys/employee"),
        menu: "employee",
        meta: {
          title: "系统用户管理",
        },
      },
      {
        path: 'role',
        name: '角色管理',
        component: ()=> import("@/views/sys/role"),
        menu: "role",
        meta: {
          title: "角色管理",
        }
      },
      {
        path: 'dict',
        name: '数据字典',
        component: ()=> import("@/views/sys/dict"),
        menu: "dict",
        meta: {
          title: "数据字典",
        }
      },
      {
        path: 'code',
        name: '代码生成',
        component: ()=> import("@/views/sys/tables"),
        menu: "code",
        meta: {
          title: "代码生成",
        }
      }
    ]
  },



]


